const getTasksByAuthor = require("./index.js").getTasksByAuthor;

(async () => {
    const authors = [
        "Mhurd",
        "Ryasmeen"
    ]
    const createdStart = "2021-03-18"
    const createdEnd = "2021-03-19"

    const tasksByAuthor = await getTasksByAuthor(authors, createdStart, createdEnd)

    console.log(`\nResults:\n\n${JSON.stringify(tasksByAuthor, null, 2)}`)
    console.log(`\nSummary:\n\nDates: ${createdStart} to ${createdEnd}\nAuthors: ${authors.join(', ')}\n`)
    console.log(`Tasks Authored:`)
    authors.forEach(author => {
      console.log(`\t${author}: ${tasksByAuthor[author] ? tasksByAuthor[author].length : 0}`)
    })
})()
