const fetch = require('cross-fetch');

const encodeToURLParameters = (json) => new URLSearchParams(json).toString()

const postOptions = () => {
    return {
        'method': 'POST',
        'Content-Type': 'application/json; charset=utf-8', 
        'Accept': 'application/json'   
    }
}

// indexedArgs('phids', ['a', 'b', 'c']) will output {phids[0]: "a", phids[1]: "b", phids[2]: "c"} 
const indexedArgs = (property, values) => {
    return values.reduce((obj, item, index) => {
        return {...obj, ...{ [`${property}[${index}]`]: item}}
    }, {})
}

// scopedIndexedArgs('constraints', 'assigned', ['a', 'b', 'c']) will output {constraints[assigned][0]: "a", constraints[assigned][1]: "b", constraints[assigned][2]: "c"} 
const scopedIndexedArgs = (scope, property, values) => {
    return values.reduce((obj, item, index) => {
        return {...obj, ...{ [`${scope}[${property}][${index}]`]: item}}
    }, {})
}

const pagingConduitFetch = async (url, body) => {
    let cursor, output
    try {
        do {
            if (cursor && cursor.after) {
                body.after = cursor.after
            }
            const response = await fetch(url, {
                ...postOptions(),
                body: encodeToURLParameters(body)
            })
            const json = await response.json()
            if (json.result.data) {
                output = [...(output || []), ...json.result.data]
            } else {
                output = {...(output || {}), ...json.result}
            }
            cursor = json.result.cursor
        } while (cursor && cursor.after)
    } catch (error) {
        // TODO: retry once on error?
        console.log(error)
    }
    return output
}

const flatten = arrayOfArrays => [].concat.apply([], arrayOfArrays)

function timestampToLocaleString(timestamp) {
  let date = new Date(timestamp * 1000)
  let options = {
    weekday: 'short', 
    year: 'numeric', 
    month: 'short', 
    day: 'numeric', 
    hour: 'numeric', 
    minute: 'numeric', 
    hour12: true
  }
  return date.toLocaleString('en-US', options)
}

// YYYY-MM-DD to unix timestamp
function dateToUnixTimestamp(dateString) {
  let date = new Date(dateString);
  let unixTimestamp = Math.floor(date.getTime() / 1000);
  return unixTimestamp;
}

module.exports = {
  pagingConduitFetch,
  indexedArgs,
  scopedIndexedArgs,
  flatten,
  timestampToLocaleString,
  dateToUnixTimestamp
};
