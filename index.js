const { pagingConduitFetch, indexedArgs, scopedIndexedArgs, flatten, timestampToLocaleString, dateToUnixTimestamp } = require('./utility.js');

const getTasksByAuthor = async (usernames, createdStart, createdEnd, url = process.env.PHABRICATOR_URL, token = process.env.CONDUIT_TOKEN) => {
    const authors = await getUserPHIDsForUsernames(usernames, url, token)
    const authorPHIDs = Object.keys(authors)
    const tasks = await pagingConduitFetch(
        `${url}/api/maniphest.search`,
        {
          'api.token': token,
          'constraints[createdStart]': dateToUnixTimestamp(createdStart),
          'constraints[createdEnd]': dateToUnixTimestamp(createdEnd),
          ...scopedIndexedArgs('constraints', 'authorPHIDs', authorPHIDs)
        }
    )
    return tasks
        .map(task => taskCleaner(task, authors))
        .reduce(tasksByAuthor, {})
}

const tasksByAuthor = (accumulator, task) => {
    const author = task.author
    if (accumulator[author] === undefined) {
        accumulator[author] = [task]
    } else {
        accumulator[author].push(task)
    }
    return accumulator
}

const taskCleaner = (task, authors) => {
    const authorInfo = authors[task.fields.authorPHID]
    return {
        id: `T${task.id}`,
        title: task.fields.name,
        author: authorInfo.username,
        created: timestampToLocaleString(task.fields.dateCreated),
        closed: task.fields.dateClosed ? timestampToLocaleString(task.fields.dateClosed) : null,
        subtype: task.fields.subtype,
        status: task.fields.status.value
    }
}

const getUserPHIDsForUsernames = async (usernames, url, token) => {
    const data = await pagingConduitFetch(
        `${url}/api/user.search`,
        {
          'api.token': token,
          ...scopedIndexedArgs('constraints', 'usernames', usernames)
        }
    )
    return data.reduce((accumulator, current) => {
      accumulator[current.phid] = {
        username: current.fields.username,
        realName: current.fields.realName,
      };
      return accumulator;
    }, {});
};

module.exports = {
  getTasksByAuthor
};
