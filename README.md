Quick script for getting numbers of Phabricator tasks authored by specific users within a date range

To use:

- set the `CONDUIT_TOKEN` in the `Dockerfile`
- adjust the `authors`, `createdStart`, `createdEnd` variables in `run.js`
- run `make`