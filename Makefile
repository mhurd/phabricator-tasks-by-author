.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	-make stop
	-make remove
	make prepare
	make run

.PHONY: prepare
prepare:
	docker build -t phabricator-tasks-by-author .

.PHONY: remove
remove:
	docker rm phabricator-tasks-by-author

.PHONY: stop
stop:
	docker stop phabricator-tasks-by-author

.PHONY: run
run:
	docker run -it --rm phabricator-tasks-by-author node /app/run.js

